#pragma once

#include "Includes.h"



namespace TextureParameter {

	enum class Name {

		WRAP_S,
		WRAP_T,
		WRAP_R,

		UPSAMPLE_FILTERING,
		DOWNSAMPLE_FILTERING
	};

	enum class Value {

		NEAREST,
		LINEAR,

		CLAMP_TO_EDGE,
		REPEAT
	};

	enum class CubemapSide {
		POSITIVE_X,
		POSITIVE_Y,
		POSITIVE_Z, 
		NEGATIVE_X,
		NEGATIVE_Y,
		NEGATIVE_Z
	};
}

struct TextureParameterPair {

	TextureParameter::Name name;
	TextureParameter::Value value;
};




