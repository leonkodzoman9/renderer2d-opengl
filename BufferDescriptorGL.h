#pragma once

#include "Includes.h"



class BufferDescriptorGL {

public:

	uint32_t ID;

public:

	BufferDescriptorGL();
	~BufferDescriptorGL();

	BufferDescriptorGL(const BufferDescriptorGL& rhs) = delete;
	BufferDescriptorGL& operator=(const BufferDescriptorGL& rhs) = delete;
	BufferDescriptorGL(BufferDescriptorGL&& rhs) noexcept;
	BufferDescriptorGL& operator=(BufferDescriptorGL&& rhs) noexcept;
};
