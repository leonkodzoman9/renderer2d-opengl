#include "TextureParameterGL.h"



uint32_t TextureParameterGL::getParameterNameGL(TextureParameter::Name name) {

	switch (name) {
	case TextureParameter::Name::WRAP_S:				return GL_TEXTURE_WRAP_S;
	case TextureParameter::Name::WRAP_T:				return GL_TEXTURE_WRAP_T;
	case TextureParameter::Name::WRAP_R:				return GL_TEXTURE_WRAP_R;
	case TextureParameter::Name::UPSAMPLE_FILTERING:	return GL_TEXTURE_MAG_FILTER;
	case TextureParameter::Name::DOWNSAMPLE_FILTERING:	return GL_TEXTURE_MIN_FILTER;
	}

	return 0;
}
uint32_t TextureParameterGL::getParameterValueGL(TextureParameter::Value value) {

	switch (value) {
	case TextureParameter::Value::NEAREST:			return GL_NEAREST;
	case TextureParameter::Value::LINEAR:			return GL_LINEAR;
	case TextureParameter::Value::CLAMP_TO_EDGE:	return GL_CLAMP_TO_EDGE;
	case TextureParameter::Value::REPEAT:			return GL_REPEAT;
	}

	return 0;
}

uint32_t TextureParameterGL::getCubemapSide(TextureParameter::CubemapSide side) {

	switch (side) {
	case TextureParameter::CubemapSide::POSITIVE_X: return GL_TEXTURE_CUBE_MAP_POSITIVE_X;
	case TextureParameter::CubemapSide::POSITIVE_Y: return GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
	case TextureParameter::CubemapSide::POSITIVE_Z: return GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
	case TextureParameter::CubemapSide::NEGATIVE_X: return GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
	case TextureParameter::CubemapSide::NEGATIVE_Y: return GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
	case TextureParameter::CubemapSide::NEGATIVE_Z:	return GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
	}

	return 0;
}
