#pragma once

#include "Includes.h"

#include "Timer.h"



class Scene {

public:

protected:

	float frametime = 0.001f;
	float gametime = 0.0f;

private:

	bool running = true;

	MultiTimer frametimeTimer;
	Timer gametimeTimer;

public:

	Scene() = default;
	virtual ~Scene() = default;

	void start();
	void quit();

	virtual void onStart() = 0;
	virtual void onUpdate() = 0;
};