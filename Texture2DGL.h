#pragma once

#include "Includes.h"

#include "Vector.h"

#include "TextureParameter.h"



class Texture2DGL {

public:

    uint32_t ID;

public:

    Texture2DGL();
    ~Texture2DGL();

    Texture2DGL(const Texture2DGL& rhs) = delete;
    Texture2DGL& operator=(const Texture2DGL& rhs) = delete;
    Texture2DGL(Texture2DGL&& rhs) noexcept;
    Texture2DGL& operator=(Texture2DGL&& rhs) noexcept;

    void setData(void* data, Vector2i size);
    void updateData(void* data, Vector2i offset, Vector2i size);

    void useLinerFiltering();
    void useNearestFiltering();
};


