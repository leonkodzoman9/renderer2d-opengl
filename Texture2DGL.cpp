#include "Texture2DGL.h"

#include "Vector.h"

#include "TextureParameterGL.h"



Texture2DGL::Texture2DGL() {

    glCreateTextures(GL_TEXTURE_2D, 1, &this->ID);

    glTextureParameteri(this->ID, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(this->ID, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}
Texture2DGL::Texture2DGL(Texture2DGL&& rhs) noexcept {

    glDeleteTextures(1, &this->ID);
    this->ID = rhs.ID;
    rhs.ID = 0;
}
Texture2DGL& Texture2DGL::operator=(Texture2DGL&& rhs) noexcept {

    glDeleteTextures(1, &this->ID);
    this->ID = rhs.ID;
    rhs.ID = 0;

    return *this;
}
Texture2DGL::~Texture2DGL() {

    glDeleteTextures(1, &this->ID);
}

void Texture2DGL::setData(void* data, Vector2i size) {
    
    glBindTexture(GL_TEXTURE_2D, this->ID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
}
void Texture2DGL::updateData(void* data, Vector2i offset, Vector2i size) {
    
    glTextureSubImage2D(this->ID, 0, offset.x, offset.y, size.x, size.y, GL_RGBA, GL_UNSIGNED_BYTE, data);
}

void Texture2DGL::useLinerFiltering() {

    glTextureParameteri(this->ID, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureParameteri(this->ID, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}
void Texture2DGL::useNearestFiltering() {

    glTextureParameteri(this->ID, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTextureParameteri(this->ID, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

