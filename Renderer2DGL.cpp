#include "Renderer2DGL.h"

#include "Window.h"



Renderer2DGL::Renderer2DGL() {
	
	this->shaderEllipse.addShader("shaderEllipse.vert", GL_VERTEX_SHADER);
	this->shaderEllipse.addShader("shaderEllipse.frag", GL_FRAGMENT_SHADER);
	this->shaderEllipse.createProgram();

	this->shaderRectangle.addShader("shaderRectangle.vert", GL_VERTEX_SHADER);
	this->shaderRectangle.addShader("shaderRectangle.frag", GL_FRAGMENT_SHADER);
	this->shaderRectangle.createProgram();

	this->shaderText.addShader("shaderText.vert", GL_VERTEX_SHADER);
	this->shaderText.addShader("shaderText.frag", GL_FRAGMENT_SHADER);
	this->shaderText.createProgram();
}

Renderer2DGL& Renderer2DGL::getInstance() {

	static Renderer2DGL instance;

	return instance;
}



void Renderer2DGL::drawLine(Line line, Vector4f color, float thickness) {

	Vector2f diff = line.end - line.start;
	Vector2f size = Vector2f(diff.magnitude() + thickness, thickness);
	float angle = std::atan2(diff.y, diff.x);

	this->drawRectangle({ .center = (line.start + line.end) / 2, .size = size, .angle = angle }, { .fillColor = color, .outlineColor = color }, 0);
}
void Renderer2DGL::drawLine2(Line line, Vector4f color, float thickness) {

	Vector2f diff = line.end - line.start;
	Vector2f size = Vector2f(diff.magnitude(), thickness);
	float angle = std::atan2(diff.y, diff.x);

	this->drawRectangle({ .center = (line.start + line.end) / 2, .size = size, .angle = angle }, { .fillColor = color, .outlineColor = color }, 0);

	this->drawCircle({ .center = line.start, .radius = thickness / 2 }, { .fillColor = color, .outlineColor = color }, 0);
	this->drawCircle({ .center = line.end, .radius = thickness / 2 }, { .fillColor = color, .outlineColor = color }, 0);
}



int64_t nCr(int64_t n, int64_t k) {

	if (k > n) { return 0; };
	if (k * 2 > n) { k = n - k; };
	if (k == 0) { return 1; };

	int64_t result = n;
	for (int i = 2; i <= k; i++) {
		result *= (n - i + 1);
		result /= i;
	}

	return result;
}

void Renderer2DGL::drawBezierCurve(BezierCurve curve, Vector4f color, float thickness, int steps) {

	std::vector<Vector2f> points(steps);

	int n = curve.points.size() - 1;

	for (int s = 0; s < steps; s++) {

		float t = s / (float)(steps - 1);

		for (int i = 0; i <= n; i++) {
			points[s] += curve.points[i] * nCr(n, i) * std::pow(1.0f - t, n - i) * std::pow(t, i);
		}
	}

	for (int i = 0; i < steps - 1; i++) {
		this->drawLine({ .start = points[i], .end = points[i + 1] }, color, thickness);
	}
}

void Renderer2DGL::drawBezierCurve2(BezierCurve curve, Vector4f color, float thickness, int steps) {

	std::vector<Vector2f> points(steps);

	int n = curve.points.size() - 1;

	for (int s = 0; s < steps; s++) {

		float t = s / (float)(steps - 1);

		for (int i = 0; i <= n; i++) {
			points[s] += curve.points[i] * nCr(n, i) * std::pow(1.0f - t, n - i) * std::pow(t, i);
		}
	}

	for (int i = 0; i < steps - 1; i++) {
		this->drawLine2({ .start = points[i], .end = points[i + 1] }, color, thickness);
	}
}

void Renderer2DGL::drawRectangle(Rectangle rectangle, ShapeColor shapeColor, float outlineWidth) {

	float quadWidth = rectangle.size.magnitude() + 1;
	float lengthToPointsOuter = rectangle.size.magnitude() / 2;
	float lengthToPointsInner = (rectangle.size - outlineWidth * 2).magnitude() / 2;

	float rectangleAngleOuter = std::atan2(rectangle.size.y, rectangle.size.x);
	float rectangleAngleInner = std::atan2(rectangle.size.y - outlineWidth * 2, rectangle.size.x - outlineWidth * 2);

	std::array<Vector2f, 4> verticesOuter;
	verticesOuter[0] = rectangle.center + GML::FromPolar(lengthToPointsOuter, rectangle.angle + rectangleAngleOuter);
	verticesOuter[1] = rectangle.center + GML::FromPolar(lengthToPointsOuter, rectangle.angle - rectangleAngleOuter);
	verticesOuter[2] = rectangle.center - GML::FromPolar(lengthToPointsOuter, rectangle.angle + rectangleAngleOuter);
	verticesOuter[3] = rectangle.center - GML::FromPolar(lengthToPointsOuter, rectangle.angle - rectangleAngleOuter);

	std::array<Vector2f, 4> verticesInner;
	verticesInner[0] = rectangle.center + GML::FromPolar(lengthToPointsInner, rectangle.angle + rectangleAngleInner);
	verticesInner[1] = rectangle.center + GML::FromPolar(lengthToPointsInner, rectangle.angle - rectangleAngleInner);
	verticesInner[2] = rectangle.center - GML::FromPolar(lengthToPointsInner, rectangle.angle + rectangleAngleInner);
	verticesInner[3] = rectangle.center - GML::FromPolar(lengthToPointsInner, rectangle.angle - rectangleAngleInner);


	this->shaderRectangle.use();
	this->setOrthographicMatrix(this->shaderRectangle);
	this->shaderRectangle.setVector2f("center", rectangle.center);
	this->shaderRectangle.setFloat("quadWidth", quadWidth);

	this->shaderRectangle.setVector2fArray("verticesOuter", verticesOuter.data(), 4);
	this->shaderRectangle.setVector2fArray("verticesInner", verticesInner.data(), 4);

	this->shaderRectangle.setVector4f("fillColor", shapeColor.fillColor);
	this->shaderRectangle.setVector4f("outlineColor", shapeColor.outlineColor);


	glBindVertexArray(this->emptyDescriptor.ID);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void Renderer2DGL::drawCircle(Circle circle, ShapeColor shapeColor, float outlineWidth) {

	this->drawEllipse({ .center = circle.center, .radii = Vector2f(circle.radius) }, shapeColor, outlineWidth);
}
void Renderer2DGL::drawEllipse(Ellipse ellipse, ShapeColor shapeColor, float outlineWidth) {

	float quadWidth = 2 * ellipse.radii.magnitude() + 1;
	float centerToFocusDistanceOuter = std::sqrt(std::pow(ellipse.radii.x, 2) - std::pow(ellipse.radii.y, 2));
	float centerToFocusDistanceInner = std::sqrt(std::pow(ellipse.radii.x - outlineWidth, 2) - std::pow(ellipse.radii.y - outlineWidth, 2));

	Vector2f focus1Outer = ellipse.center + GML::FromPolar(centerToFocusDistanceOuter, ellipse.angle);
	Vector2f focus2Outer = ellipse.center - GML::FromPolar(centerToFocusDistanceOuter, ellipse.angle);
	Vector2f focus1Inner = ellipse.center + GML::FromPolar(centerToFocusDistanceInner, ellipse.angle);
	Vector2f focus2Inner = ellipse.center - GML::FromPolar(centerToFocusDistanceInner, ellipse.angle);

	this->shaderEllipse.use();
	this->setOrthographicMatrix(this->shaderEllipse);
	this->shaderEllipse.setVector2f("center", ellipse.center);
	this->shaderEllipse.setFloat("quadWidth", quadWidth);

	this->shaderEllipse.setVector2f("focus1Outer", focus1Outer);
	this->shaderEllipse.setVector2f("focus2Outer", focus2Outer);
	this->shaderEllipse.setVector2f("focus1Inner", focus1Inner);
	this->shaderEllipse.setVector2f("focus2Inner", focus2Inner);
	this->shaderEllipse.setFloat("aOuter", ellipse.radii.x);
	this->shaderEllipse.setFloat("aInner", ellipse.radii.x - outlineWidth);

	this->shaderEllipse.setVector4f("fillColor", shapeColor.fillColor);
	this->shaderEllipse.setVector4f("outlineColor", shapeColor.outlineColor);

	glBindVertexArray(this->emptyDescriptor.ID);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}




float getTextWidth(FontGL& font, std::string& text, float height) {

	float scale = height / font.getHeight();

	float textWidth = 0;
	for (int i = 0; i < text.size(); i++) {
		Symbol& symbol = font.getSymbols()[(uint8_t)text[i]];
		textWidth += (symbol.size.x + symbol.offset.x) * scale;;
	}

	return textWidth;
}

std::array<float, 9> offsetX = { 0, -0.5, -1, 0, -0.5, -1 , 0, -0.5, -1 };
std::array<float, 9> offsetY = { 1, 1, 1, 0.5, 0.5, 0.5, 0, 0, 0 };

std::array<Vector2f, 4> positionCoefficients = { Vector2f(0, -1), Vector2f(1, -1), Vector2f(1, 0), Vector2f(0, 0) };
std::array<Vector2f, 4> uvCoefficients = { Vector2f(0, 0), Vector2f(1, 0), Vector2f(1, 1), Vector2f(0, 1) };

void Renderer2DGL::drawText(FontGL& font, TextInformation2D information) {

	float scale = information.height / font.getHeight();
	float textWidth = getTextWidth(font, information.text, information.height);

	float textHeight = 0;
	for (char& c : information.text) {
		textHeight = std::max(textHeight, font.getSymbols()[c].size.y);
	}

	Vector2f offset = Vector2f(offsetX[(int)information.alignment], offsetY[(int)information.alignment]) * Vector2f(textWidth, textHeight * scale);



	this->shaderText.use();
	this->setOrthographicMatrix(this->shaderText);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, font.getAtlas().ID);

	glBindVertexArray(this->emptyDescriptor.ID);

	float xOffset = 0;
	for (int i = 0; i < information.text.size(); i++) {

		Symbol& symbol = font.getSymbols()[(uint8_t)information.text[i]];

		Vector2f position = information.position + Vector2f(xOffset + symbol.offset.x, symbol.size.y - symbol.offset.y) * scale + offset;

		std::array<Vector4f, 4> positions;
		std::array<Vector2f, 4> uvs;

		positions[0] = Vector4f(position + positionCoefficients[0] * symbol.size * scale);
		positions[1] = Vector4f(position + positionCoefficients[1] * symbol.size * scale);
		positions[2] = Vector4f(position + positionCoefficients[2] * symbol.size * scale);
		positions[3] = Vector4f(position + positionCoefficients[3] * symbol.size * scale);

		uvs[0] = symbol.uv + uvCoefficients[0] * symbol.size / (font.getHeight() * 16);
		uvs[1] = symbol.uv + uvCoefficients[1] * symbol.size / (font.getHeight() * 16);
		uvs[2] = symbol.uv + uvCoefficients[2] * symbol.size / (font.getHeight() * 16);
		uvs[3] = symbol.uv + uvCoefficients[3] * symbol.size / (font.getHeight() * 16);

		xOffset += symbol.size.x + symbol.offset.x;

		this->shaderText.setVector4fArray("positions", positions.data(), 4);
		this->shaderText.setVector2fArray("uvs", uvs.data(), 4);
		this->shaderText.setVector4f("color", information.color);

		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	}
}



void Renderer2DGL::setOrthographicMatrix(ShaderProgramGL& shader) {

	Vector2f windowSize = Window::getInstance().size;
	Matrix4f orthographic = GML::Orthographic(0.0f, windowSize.x, 0.0f, windowSize.y, -1.0f, 1.0f);

	shader.setMatrix4f("orthographic", orthographic, true);
}




