#pragma once

#include "Includes.h"

#include "TextureParameter.h"



namespace TextureParameterGL {

	uint32_t getParameterNameGL(TextureParameter::Name name);
	uint32_t getParameterValueGL(TextureParameter::Value value);
	uint32_t getCubemapSide(TextureParameter::CubemapSide side);
}

