#pragma once

#include "Includes.h"



class Keyboard {

private:

	std::array<bool, GLFW_KEY_LAST + 1> currentState;
	std::array<bool, GLFW_KEY_LAST + 1> previousState;

	std::array<std::pair<float, float>, GLFW_KEY_LAST + 1> pressTransitionCounter;
	std::array<std::pair<float, float>, GLFW_KEY_LAST + 1> releaseTransitionCounter;

	float currentTime;

public:

	static Keyboard& getInstance();

	void update();
	void reset();

	bool held(uint32_t key) const;
	bool pressed(uint32_t key) const;
	bool released(uint32_t key) const;

	float heldFor(uint32_t key) const;
	float releasedFor(uint32_t key) const;

	float pressDelta(uint32_t key) const;
	float releaseDelta(uint32_t key) const;

private:

	Keyboard();
	Keyboard(const Keyboard& rhs) = delete;
	Keyboard& operator=(const Keyboard& rhs) = delete;
	Keyboard(Keyboard&& rhs) noexcept = delete;
	Keyboard& operator=(Keyboard&& rhs) noexcept = delete;

	void updatePressTransitionCounter(uint32_t key, float currentTime);
	void updateReleaseTransitionCounter(uint32_t key, float currentTime);
};


