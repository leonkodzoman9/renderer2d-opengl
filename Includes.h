#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <fstream>

#include <cmath>
#include <ctime>

#include <chrono>
#include <thread>
#include <algorithm>
#include <random>
#include <numeric>
#include <numbers>
#include <execution>

#include <string>
#include <sstream>

#include <array>
#include <vector>
#include <map>
#include <unordered_map>
#include <queue>
#include <set>
#include <span>

#include <type_traits>
#include <emmintrin.h>


typedef uint32_t ByteCount;
typedef uint64_t EntityIDType;

constexpr float TextHeightFactor = 0.6;
constexpr float SliderSizeFactor = 0.8;


