#version 460 core

layout (location = 0) out vec4 finalColor;

uniform vec2[4] verticesOuter;
uniform vec2[4] verticesInner;

uniform vec2 center;

uniform vec4 fillColor;
uniform vec4 outlineColor;

in vec2 fragPos;

void main() {

	vec2 sidesOuter[4];
	sidesOuter[0] = verticesOuter[1] - verticesOuter[0];
	sidesOuter[1] = verticesOuter[2] - verticesOuter[1];
	sidesOuter[2] = verticesOuter[3] - verticesOuter[2];
	sidesOuter[3] = verticesOuter[0] - verticesOuter[3];
	
	bool isInsideOuter = true;
	for (int i = 0; i < 4; i++) {
		
		vec2 side = verticesOuter[(i + 1) % 4] - verticesOuter[i];
		vec2 vector = fragPos - verticesOuter[i];

		if (cross(vec3(vector, 0), vec3(side, 0)).z < 0) {
			isInsideOuter = false;
		}
	}

	bool isInsideInner = true;
	for (int i = 0; i < 4; i++) {
		
		vec2 side = verticesInner[(i + 1) % 4] - verticesInner[i];
		vec2 vector = fragPos - verticesInner[i];

		if (cross(vec3(vector, 0), vec3(side, 0)).z < 0) {
			isInsideInner = false;
		}
	}
	
	if (!isInsideOuter) {
		discard;
	}

	if (isInsideInner && isInsideOuter) {
		finalColor = fillColor;
	} 
	if (!isInsideInner && isInsideOuter) {
		finalColor = outlineColor;
	}


}


