#pragma once

#include "Includes.h"
#include "ShaderProgramGL.h"
#include "BufferDescriptorGL.h"
#include "FontGL.h"
#include "TextInformation.h"



struct Ellipse {

	Vector2f center;
	Vector2f radii = Vector2f(200, 100);
	float angle = 0;
};

struct Circle {

	Vector2f center;
	float radius;
};

struct Rectangle {

	Vector2f center;
	Vector2f size;
	float angle = 0;
};

struct Line {

	Vector2f start;
	Vector2f end;
};

struct BezierCurve {

	std::vector<Vector2f> points;
};



struct ShapeColor {

	Vector4f fillColor = Vector4f(0.7, 0.7, 0.7, 1);
	Vector4f outlineColor = Vector4f(0.3, 0.3, 0.3, 1);
};



class Renderer2DGL {

private:

	BufferDescriptorGL emptyDescriptor;

	ShaderProgramGL shaderEllipse;
	ShaderProgramGL shaderRectangle;
	ShaderProgramGL shaderLine;
	ShaderProgramGL shaderText;

	Renderer2DGL();

public:

	static Renderer2DGL& getInstance();

	void drawLine(Line line, Vector4f color, float thickness = 1);
	void drawLine2(Line line, Vector4f color, float thickness = 1);

	void drawBezierCurve(BezierCurve curve, Vector4f color, float thickness = 1, int steps = 20);
	void drawBezierCurve2(BezierCurve curve, Vector4f color, float thickness = 1, int steps = 20);

	void drawRectangle(Rectangle rectangle, ShapeColor shapeColor, float outlineWidth = 1);

	void drawCircle(Circle circle, ShapeColor shapeColor, float outlineWidth = 1);
	void drawEllipse(Ellipse ellipse, ShapeColor shapeColor, float outlineWidth = 1);

	void drawText(FontGL& font, TextInformation2D information);

private:

	void setOrthographicMatrix(ShaderProgramGL& shader);
};