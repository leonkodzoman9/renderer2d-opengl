#include "Keyboard.h"

#include "Window.h"



Keyboard& Keyboard::getInstance() {

	// A static instance becomes a singleton, first time initialized, other times just returned
	static Keyboard instance;

	return instance;
}

void Keyboard::update() {

	GLFWwindow* window = Window::getInstance().getWindow();

	this->currentTime = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch()).count() / 1'000'000.0f;

	for (int i = 0; i <= GLFW_KEY_LAST; i++) {

		this->previousState[i] = this->currentState[i];
		this->currentState[i] = glfwGetKey(window, i) == GLFW_PRESS;

		bool currentState = this->currentState[i];
		bool previousState = this->previousState[i];

		if (!previousState && currentState) {
			this->updatePressTransitionCounter(i, currentTime);
		}

		if (previousState && !currentState) {
			this->updateReleaseTransitionCounter(i, currentTime);
		}
	}
}
void Keyboard::reset() {

	std::fill(this->currentState.begin(), this->currentState.end(), 0);
	std::fill(this->previousState.begin(), this->previousState.end(), 0);

	this->currentTime = 0;
	std::fill(this->pressTransitionCounter.begin(), this->pressTransitionCounter.end(), std::pair<float, float>(0, 0));
	std::fill(this->releaseTransitionCounter.begin(), this->releaseTransitionCounter.end(), std::pair<float, float>(0, 0));
}

bool Keyboard::held(uint32_t key) const {

	return this->currentState[key];
}
bool Keyboard::pressed(uint32_t key) const {

	return this->currentState[key] && !this->previousState[key];
}
bool Keyboard::released(uint32_t key) const {

	return !this->currentState[key] && this->previousState[key];
}

float Keyboard::heldFor(uint32_t key) const {

	return this->held(key) ? this->currentTime - this->pressTransitionCounter[key].second : 0;
}
float Keyboard::releasedFor(uint32_t key) const {

	return !this->held(key) ? this->currentTime - this->releaseTransitionCounter[key].second : 0;
}

float Keyboard::pressDelta(uint32_t key) const {

	return this->pressTransitionCounter[key].second - this->pressTransitionCounter[key].first;
}
float Keyboard::releaseDelta(uint32_t key) const {

	return this->releaseTransitionCounter[key].second - this->releaseTransitionCounter[key].first;
}



Keyboard::Keyboard() {

	this->reset();
};

void Keyboard::updatePressTransitionCounter(uint32_t key, float currentTime) {

	this->pressTransitionCounter[key].first = this->pressTransitionCounter[key].second;
	this->pressTransitionCounter[key].second = currentTime;
}
void Keyboard::updateReleaseTransitionCounter(uint32_t key, float currentTime) {

	this->releaseTransitionCounter[key].first = this->releaseTransitionCounter[key].second;
	this->releaseTransitionCounter[key].second = currentTime;
}
