#pragma once

#include "Includes.h"

#include "Vector.h"



class Image {

public:

    Vector2i size;
    std::vector<Vector<uint8_t, 4>> data;

public:

    Image(std::string filepath);
};

