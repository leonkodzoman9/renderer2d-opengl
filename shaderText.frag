#version 460 core

layout (location = 0) out vec4 finalColor;
layout (location = 1) out vec4 bloomColor;

layout (binding = 0) uniform sampler2D textureSampler;

in FragmentData {
    vec2 uv;
    vec4 color;
} fragmentData;



void main() {   

    float alpha = texture(textureSampler, fragmentData.uv).r;
    
    if (alpha < 0.5) {
        discard;
    }

    vec4 color = fragmentData.color * vec4(1.0, 1.0, 1.0, alpha) * 0.999;

    finalColor = color / (vec4(1) - color);
    finalColor.w = alpha;

    bloomColor = vec4(0);
}  