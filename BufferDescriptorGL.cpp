#include "BufferDescriptorGL.h"



BufferDescriptorGL::BufferDescriptorGL() {

    glCreateVertexArrays(1, &this->ID);
}
BufferDescriptorGL::~BufferDescriptorGL() {

    glDeleteVertexArrays(1, &this->ID);
}

BufferDescriptorGL::BufferDescriptorGL(BufferDescriptorGL&& rhs) noexcept {

    if (this != &rhs) {

        this->ID = std::exchange(rhs.ID, 0);
    }
}
BufferDescriptorGL& BufferDescriptorGL::operator = (BufferDescriptorGL&& rhs) noexcept {

    if (this != &rhs) {

        glDeleteVertexArrays(1, &this->ID);

        this->ID = std::exchange(rhs.ID, 0);
    }

    return *this;
}


