#include "FontGL.h"

#include <ft2build.h>
#include FT_FREETYPE_H  



FontGL::FontGL(std::string fontName, int height) {

    this->height = height;

    FT_Library library;
    if (FT_Init_FreeType(&library)) {
        printf("Could not initialize FreeType.\n");
        throw;
    }

    FT_Face face;
    if (FT_New_Face(library, ("C:/Windows/Fonts/" + fontName + ".ttf").data(), 0, &face)) {
        printf("Could not load font.\n");
        throw;
    }

    FT_Set_Pixel_Sizes(face, 0, height);

    std::vector<uint8_t> textureData((uint64_t)height * (uint64_t)height * 256ull);

    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {

            int index = i * 16 + j;
            Vector2i textureDataOffset = Vector2i(j, i) * height;

            if (FT_Load_Char(face, index, FT_LOAD_RENDER)) {
                continue;
            }

            Vector2i size(face->glyph->bitmap.width, face->glyph->bitmap.rows);

            for (int k = 0; k < size.y; k++) {
                int index = (textureDataOffset.y + k) * height * 16 + textureDataOffset.x;
                std::memcpy(textureData.data() + index, face->glyph->bitmap.buffer + size.x * k, size.x);
            }

            if (size.x == 0) {
                size.x = height / 3;
            }

            this->symbols[index].uv = Vector2f(j, i) / 16;
            this->symbols[index].size = size;
            this->symbols[index].offset = Vector2f(face->glyph->bitmap_left, face->glyph->bitmap_top);
        }
    }

    FT_Done_Face(face);
    FT_Done_FreeType(library);

    std::vector<Vector<uint8_t, 4>> atlasData(textureData.size());
    std::transform(textureData.begin(), textureData.end(), atlasData.begin(), [](uint8_t color) { return Vector4f(color, 0, 0, 1); });

    this->fontAtlas.setData(atlasData.data(), Vector2i(16, 16) * height);
    this->fontAtlas.useLinerFiltering();
}

FontGL::FontGL(FontGL&& rhs) noexcept {

    this->height = rhs.height;
    this->fontAtlas = std::move(rhs.fontAtlas);
    for (int i = 0; i < 256; i++) {
        this->symbols[i] = std::move(rhs.symbols[i]);
    }
}
FontGL& FontGL::operator=(FontGL&& rhs) noexcept {

    this->height = rhs.height;

    if (this != &rhs) {

        this->fontAtlas = std::move(rhs.fontAtlas);
        for (int i = 0; i < 256; i++) {
            this->symbols[i] = std::move(rhs.symbols[i]);
        }
    }

    return *this;
}

Texture2DGL& FontGL::getAtlas() {

    return this->fontAtlas;
}
float FontGL::getHeight() {

    return this->height;
}
std::array<Symbol, 256>& FontGL::getSymbols() {

    return this->symbols;
}

