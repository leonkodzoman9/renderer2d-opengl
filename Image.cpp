#include "Image.h"

#include "Vector.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"



Image::Image(std::string filepath) {

    int channels;
    uint8_t* data = stbi_load(filepath.data(), &this->size.x, &this->size.y, &channels, STBI_rgb_alpha);

    int pixelCount = this->size.x * this->size.y;
    
    this->data.resize(pixelCount);
    std::memcpy(this->data.data(), data, pixelCount * sizeof(Vector<uint8_t, 4>));

    stbi_image_free(data);
}

