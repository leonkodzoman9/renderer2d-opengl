#pragma once

#include "Includes.h"

#include "ShaderProgramGL.h"
#include "Texture2DGL.h"
#include "Vector.h"



struct Symbol {

    Vector2f uv;
    Vector2f size;
    Vector2f offset;
};



class FontGL {

private:

    Texture2DGL fontAtlas;

    int height = 0;
    std::array<Symbol, 256> symbols;

public:

    FontGL() = default;
    FontGL(std::string sourceName, int height = 64);
    
    FontGL(const FontGL& rhs) = delete;
    FontGL& operator=(const FontGL& rhs) = delete;
    FontGL(FontGL&& rhs) noexcept;
    FontGL& operator=(FontGL&& rhs) noexcept;

    Texture2DGL& getAtlas();
    float getHeight();
    std::array<Symbol, 256>& getSymbols();
};



