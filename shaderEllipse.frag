#version 460 core

layout (location = 0) out vec4 finalColor;

uniform vec2 focus1Outer;
uniform vec2 focus2Outer;
uniform vec2 focus1Inner;
uniform vec2 focus2Inner;
uniform float aOuter;
uniform float aInner;

uniform vec4 fillColor;
uniform vec4 outlineColor;

in vec2 fragPos;

void main() {

	float distanceOuter = length(fragPos - focus1Outer) + length(fragPos - focus2Outer);
	float distanceInner = length(fragPos - focus1Inner) + length(fragPos - focus2Inner);

	float maxDistanceOuter = 2 * aOuter;
	float maxDistanceInner = 2 * aInner;

	if (distanceOuter > maxDistanceOuter) {
		discard;
	}

	if (distanceInner < maxDistanceInner && distanceOuter < maxDistanceOuter) { 
		finalColor = fillColor;
	}
	if (distanceInner > maxDistanceInner && distanceOuter < maxDistanceOuter) { 
		finalColor = outlineColor;
	}
}


