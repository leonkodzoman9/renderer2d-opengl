#pragma once

#include "Includes.h"

#include "Vector.h"



enum class TextAlignment {
	TOPLEFT,
	MIDTOP,
	TOPRIGHT,
	MIDLEFT,
	CENTER,
	MIDRIGHT,
	BOTTOMLEFT,
	MIDBOTTOM,
	BOTTOMRIGHT
};




struct TextInformation2D {

	std::string text = "None";

	Vector2f position;
	float height = 32;

	Vector4f color = Vector4f(1);
	TextAlignment alignment = TextAlignment::CENTER;
};
