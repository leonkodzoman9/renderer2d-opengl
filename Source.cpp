#include "Includes.h"

#include "Window.h"
#include "Mouse.h"
#include "Keyboard.h"

#include "Scene.h"
#include "Renderer2DGL.h"
#include "FontGL.h"



struct Ball {

	Vector2f position;
	Vector2f velocity;
	Vector2f acceleration;

	float radius;

	Vector3f color;
};





class BallGrid {
	
public:

	int rows;
	int cols;
	float cellSize;

	Vector2f topleft;
	Vector2f bottomright;
	Vector2f area;

	std::vector<std::vector<Ball*>> balls;


	
	BallGrid() = default;
	BallGrid(Vector2f topleft, Vector2f bottomright, float cellSize) {

		this->topleft = topleft;
		this->bottomright = bottomright;
		this->area = bottomright - topleft;

		this->cellSize = cellSize;
		this->rows = this->area.x / cellSize + 1;
		this->cols = this->area.y / cellSize + 1;

		this->balls.resize(this->rows * this->cols);
	}

	void addBall(Ball* ball) {

		int row = (ball->position.y - this->topleft.y) / area.y * this->rows;
		int col = (ball->position.x - this->topleft.x) / area.x * this->cols;

		balls[row * this->cols + col].push_back(ball);
	}

	void getBalls(Ball* ball, std::vector<Ball*>& balls) {

		int row = (ball->position.y - this->topleft.y) / area.y * this->rows;
		int col = (ball->position.x - this->topleft.x) / area.x * this->cols;

		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {

				int r = row + i;
				int c = col + j;

				auto& vec = this->balls[r * this->cols + c];

				if (vec.size() > 0) {
					balls.insert(balls.end(), vec.begin(), vec.end());
				}
			}
		}

	}
};











class Example : public Scene {

private:

	FontGL font;

	std::vector<Ball> balls;
	float maxRadius;

public:

	void onStart() {

		this->font = FontGL("consola");

		Vector2f wSize = Window::getInstance().size;

		std::random_device device;
		std::mt19937 generator(device());
		std::uniform_real_distribution<> distribution;

		this->balls.resize(1000);
		for (auto& ball : this->balls) {
			ball.position = Vector2f(distribution(generator) * wSize.x, distribution(generator) * wSize.y);
			ball.radius = 8 + (distribution(generator) * 4 - 2) * 0;
			ball.color = Vector3f(distribution(generator), distribution(generator), distribution(generator));
		}

		this->maxRadius = std::max_element(this->balls.begin(), this->balls.end(), [](Ball& ball1, Ball& ball2) { return ball1.radius < ball2.radius; })->radius;
	}

	void onUpdate() {

		Renderer2DGL& renderer = Renderer2DGL::getInstance();
		Mouse& mouse = Mouse::getInstance();
		Keyboard& keyboard = Keyboard::getInstance();



		Vector2f wSize = Window::getInstance().size;
		
		TextInformation2D ftText;
		ftText.text = "Frametime : " + std::to_string(this->frametime * 1000);
		ftText.position = Vector2f(5, 0);
		ftText.height = 32;
		ftText.alignment = TextAlignment::TOPLEFT;
		
		TextInformation2D fpsText;
		fpsText.text = "FPS : " + std::to_string(1 / this->frametime);
		fpsText.position = Vector2f(5, 30);
		fpsText.height = 32;
		fpsText.alignment = TextAlignment::TOPLEFT;
		
		TextInformation2D usageText;
		usageText.text = "Hold left click to force balls away";
		usageText.position = Vector2f(wSize.x / 2, 10);
		usageText.height = 32;
		usageText.alignment = TextAlignment::MIDTOP;
		
		renderer.drawText(this->font, ftText);
		renderer.drawText(this->font, fpsText);
		renderer.drawText(this->font, usageText);
		
		
		
		float dt = this->frametime;
		
		for (int i = 0; i < this->balls.size(); i++) {
		

			Ball tempBall;
			Ball& ball = this->balls[i];
		
			tempBall.position = ball.position + ball.velocity * dt + ball.acceleration * (dt * dt * 0.5);
		
			tempBall.acceleration = Vector2f(0, 5);
			if (mouse.held(GLFW_MOUSE_BUTTON_LEFT)) {
				Vector2f diff = mouse.position() - ball.position;
				tempBall.acceleration -= diff / diff.magnitude2() * 10000;
			}
			tempBall.acceleration *= 100;
		
			tempBall.velocity = ball.velocity + (ball.acceleration + tempBall.acceleration) * (dt * 0.5);
		
			tempBall.radius = ball.radius;
			tempBall.color = ball.color;

			ball = tempBall;
		}
		
		
		for (int iterations = 0; iterations < 5; iterations++) {
		
			BallGrid grid(Vector2f(-50), wSize + 50, this->maxRadius * 2 + 3);

			for (int i = 0; i < this->balls.size(); i++) {
				grid.addBall(&this->balls[i]);
			}

			std::vector<Ball*> balls;

			for (int i = 0; i < this->balls.size(); i++) {

				balls.clear();
				grid.getBalls(&this->balls[i], balls);

				for (int j = 0; j < balls.size(); j++) {
		
					Ball& ball1 = this->balls[i];
					Ball& ball2 = *balls[j];

					if (&ball2 <= &ball1) { continue; }

					Vector2f diff = ball2.position - ball1.position;
		
					float magnitude = diff.magnitude();
					float radii = ball1.radius + ball2.radius;
		
					if (magnitude < radii) {
		
						Vector2f unitDiff = GML::Normalize(diff);
						float offset = (radii - magnitude) / 2;
		
						ball1.position -= unitDiff * offset;
						ball2.position += unitDiff * offset;
		
						Vector2f vel1 = unitDiff * GML::Dot(ball1.velocity, unitDiff);
						Vector2f vel2 = unitDiff * GML::Dot(ball2.velocity, unitDiff);
		
						ball1.velocity += vel2 * 0.99 - vel1;
						ball2.velocity += vel1 * 0.99 - vel2;
					}
				}
			}
		
			for (auto& ball : this->balls) {
		
				if (ball.position.x < ball.radius) {
					ball.velocity.x *= -0.95;
					ball.position.x = ball.radius;
				}
				if (ball.position.x > wSize.x - ball.radius) {
					ball.velocity.x *= -0.95;
					ball.position.x = wSize.x - ball.radius;
				}
				if (ball.position.y < ball.radius) {
					ball.velocity.y *= -0.95;
					ball.position.y = ball.radius;
				}
				if (ball.position.y > wSize.y - ball.radius) {
					ball.velocity.y *= -0.95;
					ball.position.y = wSize.y - ball.radius;
				}
			}
		}
		
		for (auto& ball : this->balls) {
			renderer.drawCircle(
				{ .center = ball.position, .radius = ball.radius }, 
				{ .fillColor = Vector4f(ball.color / 2, 1), .outlineColor = Vector4f(ball.color, 1) }, 1);
		}
	}
};



int main() {

	Window::getInstance().initialize(Vector2i(1600, 900));
	glfwSwapInterval(0);

	Example test;
	test.start();

	return 0;
}






