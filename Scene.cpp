#include "Scene.h"

#include "Mouse.h"
#include "Keyboard.h"
#include "Window.h"



void Scene::start() {

	this->frametimeTimer = MultiTimer(100);
	this->gametimeTimer.start();

	this->onStart();

	bool isFirstFrame = true;

	while (this->running) {

		Mouse& mouse = Mouse::getInstance();
		Keyboard& keyboard = Keyboard::getInstance();

		this->frametimeTimer.update();
		this->frametime = this->frametimeTimer.getAverageInterval() / 1000.0f;
		this->gametimeTimer.stop();
		this->gametime = this->gametimeTimer.getInterval() / 1000.0f;

		glfwPollEvents();
		mouse.update();
		keyboard.update();

		if (isFirstFrame) {
			isFirstFrame = false;
			continue;
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		this->onUpdate();

		glfwSwapBuffers(Window::getInstance().getWindow());



		if (keyboard.held(GLFW_KEY_LEFT_CONTROL) && keyboard.pressed(GLFW_KEY_Q)) {
			this->quit();
		}
	}

	glfwTerminate();
}
void Scene::quit() {

	this->running = false;
}



