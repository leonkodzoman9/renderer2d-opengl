#pragma once

#include "Includes.h"

#include "Vector.h"



class Window {

public:

	Vector2i size;

private:

	std::string title;

	GLFWwindow* window;

public:

	static Window& getInstance();

	void initialize(Vector2i windowSize, const std::string& windowName = "Window");

	GLFWwindow* getWindow();

	std::string getTitle() const;
	void setTitle(const std::string& windowName);

private:

	Window();
	~Window();

	Window(const Window& rhs) = delete;
	Window& operator=(const Window& rhs) = delete;
	Window(Window&& rhs) noexcept = delete;
	Window& operator=(Window&& rhs) noexcept = delete;
};





