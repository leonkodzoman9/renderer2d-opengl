#version 460 core

uniform mat4 orthographic;

uniform vec2 center;
uniform float quadWidth;

const vec2[4] verts = vec2[4](vec2(-1, -1), vec2(-1, 1), vec2(1, 1), vec2(1, -1));

out vec2 fragPos;

void main() {

    vec2 pos = center + verts[gl_VertexID] * quadWidth / 2;

    gl_Position = orthographic * vec4(pos, 0, 1);
    
    fragPos = pos;
}  