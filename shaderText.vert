#version 460 core

uniform vec4 positions[4];
uniform vec2 uvs[4];
uniform vec4 color;

out FragmentData {
    vec2 uv;
    vec4 color;
} fragmentData;

uniform mat4 orthographic;



void main() {

    gl_Position = orthographic * vec4(positions[gl_VertexID].xyz, 1.0);
    
    fragmentData.uv = uvs[gl_VertexID];
    fragmentData.color = color;
}  